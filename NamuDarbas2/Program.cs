﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NamuDarbas2
{
    class Program
    {
        static void Main(string[] args)
        {
            NamuDarbas2.ND NamuD = new NamuDarbas2.ND();
            bool err = true;
            while (err)
            {
                
                if (NamuD.pasirinkimas_kalbos() != 0)
                {
                    NamuD.pasisveikinimas();
                    if (NamuD.inf_surinkimas() != 0)
                    {
                        err = NamuD.tikrinimas();

                        if (err == false)
                        {
                            Console.WriteLine(NamuD.bandyk());
                            string ats = NamuD.tikrinam_TN(NamuD.Readline());
                            if (NamuD.get_exit())ats="exit";
                            while (ats == "")
                            {
                                ats = NamuD.tikrinam_TN(NamuD.Readline());
                                if (NamuD.get_exit()) break;
                                if (ats == "") NamuD.get_klaida();
                            }
                            if (ats == "Taip")
                            {
                                err = true;
                            }

                            if (ats == "Ne")
                            {
                                err = false;
                            }

                        }
                        else
                        {
                            NamuD.apk_amziu();
                            Console.WriteLine(NamuD.get_atsakymas());
                        }
                    }
                   
                }
                if (NamuD.get_exit()) err = false;
                else
                {
                    Console.WriteLine(NamuD.get_pakartot());
                    string tmp;
                    do
                    {
                        tmp = NamuD.tikrinam_TN(NamuD.Readline());
                        if (tmp == "Ne") err = false;
                        if (tmp == "Taip") NamuD.clean_atsakymas();
                        if (NamuD.get_exit())
                        {
                            err = false;
                            break;
                        }
                    } while (tmp != "Ne" && tmp != "Taip");
                }

            }
            if (NamuD.get_exit()) Console.WriteLine(NamuD.get_exit_error());
            Console.ReadLine();
        }
    }
}
