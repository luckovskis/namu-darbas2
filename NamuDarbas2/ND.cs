﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NamuDarbas2
{
    class ND
    {
        private static bool Exit;
        private static int kalba;
        private static string vardas;
        private static string pavarde;
        private static int amzius;
        private static float ugis;
        private static bool lytis;
        private static string atsakymas;
        private static string[] klaida_str = new string[] { "Klaida! , pakartok", "Error! , repeat", "Ошибка! , повтори" };
        private static string[] bandyk_dar = new string[] { "Bandyti dar? Taip/Ne", "Try again? Yes/No", "Пробоватъ ешё раз? Да/Нет" };
        private static string[] ate = new string[] { "Ate", "Goodbye", "Пока" };
        private static string[] neteisingai = new string[] {"Blogai suvesti duomenys!!!", "Incorrectly data !!!", "Некорректные данные !!!" };
        private static string[] exit = new string[] { "Priverstinis programos sustabdymas", "The forced suspension of the program", "Принудительное остановление программы" };
        private static string[] pakartot = new string[] {"Ar kartosi?", "Do you repeat?", "Будиш повторять?" };


        public void clean_atsakymas()
        {
            atsakymas = "";
        }
        public string get_pakartot()
        {
            return pakartot[kalba];
        }

        public string get_exit_error()
        {
            return exit[kalba];
        }

        public bool get_exit()
        {
            return Exit;
        }

        public string get_atsakymas()
        {
            string[] metu = new string[] { "metu", "years old", "лет" };
            string[] lts_ugis = new string[] {"ūgis", "height", "рост" };
            string[] lts_moteris = new string[] {" Moteris "," Women "," Женщина " };
            string[] lts_vyras = new string[] { " Vyras ", " Man ", " Mужчина " };
           
            string ats = vardas + ' ' + pavarde + ' ' + Convert.ToString(amzius) + " "+ metu[kalba]+" , "+lts_ugis[kalba] +" "+ ugis.ToString() + " m.";
            if (lytis)
            {
                ats += ","+lts_moteris[kalba];
            }
            else
            {
                ats += ","+lts_vyras[kalba];
            }
            ats += atsakymas;
            return ats;
        }

        public string bandyk()
        {
            return   neteisingai[ND.kalba] + "\n"+bandyk_dar[ND.kalba];
        }
        public string get_klaida()
        {
            return klaida_str[ND.kalba];
        }

        public string Readline()
        {
            string tmp = Console.ReadLine();

            if (tmp.ToLower() == "exit"||tmp.ToLower()=="ate"||tmp.ToLower()=="выйти") Exit = true;
            return tmp;
        }

        public int pasirinkimas_kalbos()
        {
            Console.WriteLine("Lietuviu ivesk - LT");
            Console.WriteLine("English enter - EN");
            Console.WriteLine("Руский введи - RU");
            string klb = Readline();
            if (Exit) return 0;
            if (klb == "Lt" || klb == "lt" || klb == "lT") ND.kalba = 0;
            if (klb == "En" || klb == "en" || klb == "eN") ND.kalba = 1;
            if (klb == "Ru" || klb == "ru" || klb == "rU") ND.kalba = 2;
            return 1;
        }
        public void pasisveikinimas()
        {
            if (ND.kalba == 0) Console.WriteLine("Sveiki");
            if (ND.kalba == 1) Console.WriteLine("Hello");
            if (ND.kalba == 2) Console.WriteLine("Привет");
        }
        public int inf_surinkimas()
        {
            string[] ivesk_varda = new string[] { "Ivesk Varda", "Enter Name", "Введи Имя" };
            string[] ivesk_pavarde = new string[] { "Ivesk Pavarde", "Enter Surname", "Введи фамилию" };
            string[] ivesk_amziu = new string[] { "Kiek tau metu?", "How old are you?", "Сколко тебе лет?" };
            string[] ivesk_ugi = new string[] { "Koks tavo ugis?", "What is your height?", "Какой твой рост?" };
            string[] ivesk_lyti = new string[] { "Ar tu moteris? Taip/Ne", "Are you women? Yes/No", "Ты женьшина?  Да/Нет" };

            Console.WriteLine(ivesk_varda[ND.kalba]);
            ND.vardas = Readline();
            if (Exit) return 0;
            Console.WriteLine(ivesk_pavarde[ND.kalba]);
            ND.pavarde = Readline();
            if (Exit) return 0;
            Console.WriteLine(ivesk_amziu[ND.kalba]);
            ND.amzius = Convert.ToInt32(Readline());
            if (Exit) return 0;
            Console.WriteLine(ivesk_ugi[ND.kalba]);
            float.TryParse(Readline(), out ND.ugis);
            if (Exit) return 0;
            Console.WriteLine(ivesk_lyti[ND.kalba]);

            int klaida = 1;
            string lyt;
            do
            {
                lyt = tikrinam_TN(Readline());
                if (Exit) return 0;
                if (lyt == "Taip")
                {
                    ND.lytis = true;
                    klaida = 0;
                }

                if (lyt == "Ne")
                {
                    ND.lytis = false;
                    klaida = 0;
                }

                if (klaida == 1)
                {
                    Console.WriteLine(ND.klaida_str[ND.kalba]);
                }


            } while (klaida == 1);
            return 1;
        }

        public bool tikrinimas()
        {
            if (ND.vardas != "" && ND.pavarde != "" && ND.amzius > 0 && ND.ugis > 0.30)
			{
                return true;
            }
			else
			{
                Console.WriteLine(ND.klaida_str[ND.kalba]);
                return false;
            }
        }
        public string tikrinam_TN(string val)
        {
            string vall = val.ToLower();
            if (vall == "taip" || vall == "yes" || vall == "да")
            {
                return "Taip";
            }

            if (val == "ne" || val == "no" || val == "нет")
            {
                return "Ne";
            }
            if (vall == "teigiama" || vall == "positive" || vall == "положительнoе")
            {
                return "Taip";
            }

            if (val == "neigiama" || vall == "Negative" || val == "отрицательнoе")
            {
                return "Ne";
            }
            return "";

        }

        public int apk_amziu()
        {
        
			if(ND.amzius >0 && ND.amzius <= 10)
			{
				string[] eini_i_mokykla = new string[]{ "Ar eini i mokykla? Taip/Ne", "Do you go to school? Yes/No", "Ты идёш в школу? Да/Нет" };
                Console.WriteLine(eini_i_mokykla[ND.kalba]);
			    string ats = tikrinam_TN(Readline());
                if (Exit) return 0;
                while (ats=="")
				{
					Console.WriteLine(klaida_str[ND.kalba]);
					ats= tikrinam_TN(Readline());
                    if (Exit) return 0;
                }
				if(ats =="Taip")
				{
					if(ND.kalba==0)ND.atsakymas+=" ,eina i mokykla";
					if(ND.kalba==1)ND.atsakymas+=" ,going to school";
					if(ND.kalba==2)ND.atsakymas+=" ,идёт в школу";
					string[] kuria_klase = new string[] { "I kuria klase eini?", "Which class are you going?", "В который класс идёш?" };
                    Console.WriteLine(kuria_klase[ND.kalba]);
					
					if(ND.kalba==0)ND.atsakymas+=" , i "+(Readline())+" klase";
					if(ND.kalba==1)ND.atsakymas+=" , in Class "+(Readline());
					if(ND.kalba==2)ND.atsakymas+=" , в "+(Readline())+" класс";
                    if (Exit) return 0;

                }
				if(ats=="Ne")
				{
					string[] kiek_laukti = new string[] { "Kiek liko menesiu iki mokyklos?", "How many months before school?", "Сколько осталось месяцов до школы?" };
                    Console.WriteLine(kiek_laukti[ND.kalba]);
					
					
					if(ND.kalba==0)ND.atsakymas+= "iki mokyklos liko:"+(Readline())+"menesiu";
					if(ND.kalba==1)ND.atsakymas+= "to school after : "+(Readline())+"months";
					if(ND.kalba==2)ND.atsakymas+= "до школы осталось:"+(Readline())+"месяцов";
                    if (Exit) return 0;
                }
			}
  
			
			if(ND.amzius >10&&ND.amzius<=20)
			{
				string[] baige_mokykla = new string[] { "Ar jau baigei mokykla? Taip/Ne", "Оr finished school? Yes/No", "Ужэ законьчил школу? Да/Нет" };
                Console.WriteLine(baige_mokykla[ND.kalba]);
				string ats = tikrinam_TN(Readline());
                if (Exit) return 0;
                while (ats=="")
				{
					Console.WriteLine(klaida_str[ND.kalba]);
					ats=tikrinam_TN(Readline());
                    if (Exit) return 0;
                }
				if(ats =="Taip")
				{
					if(ND.kalba==0)ND.atsakymas+=" ,Baige mokykla";
					if(ND.kalba==1)ND.atsakymas+=" ,finished school";
					if(ND.kalba==2)ND.atsakymas+=" ,закончил школу";
					string[] istojai = new string[] { "Ar istojai mokytis? Jei taip tai kur?", "Are you enrolled? If so, then where?", "Поступил ли учитця? Если да, то куда?" };
                    Console.WriteLine(istojai[ND.kalba]);
					
					if(ND.kalba==0)ND.atsakymas+=" , istojo "+(Readline());
					if(ND.kalba==1)ND.atsakymas+=" , enrolled "+(Readline());
					if(ND.kalba==2)ND.atsakymas+=" , поступил "+(Readline());
                    if (Exit) return 0;
                }
				if(ats=="Ne")
				{
					string[] kiek_liko = new string[] { "Kiek metu liko mokytis mokykloje?", "How many years have remained in school?", "Сколько лет осталось учится в школе?" };
                    Console.WriteLine(kiek_liko[ND.kalba]);
					
					
					if(ND.kalba==0)ND.atsakymas+= "iki mokyklos liko:"+(Readline())+"menesiu";
					if(ND.kalba==1)ND.atsakymas+= "to school after : "+(Readline())+"months";
					if(ND.kalba==2)ND.atsakymas+= "до школы осталось:"+(Readline())+"месяцов";
                    if (Exit) return 0;
                }
				
			}
			
			if(ND.amzius >20&&ND.amzius<=30)
			{
				string[] gyveni_vilniuje = new string[] { "Ar gyveni Vilniuje? Taip/Ne", "Do you live in Vilnius? Yes/No", "Вы живете в Вильнюсе? Да/Нет" };
                Console.WriteLine(gyveni_vilniuje[ND.kalba]);
				string ats = tikrinam_TN(Readline());
                if (Exit) return 0;
                while (ats=="")
				{
					Console.WriteLine(klaida_str[ND.kalba]);
					ats=tikrinam_TN(Readline());
                    if (Exit) return 0;
                }
				if(ats =="Taip")
				{
					if(ND.kalba==0)ND.atsakymas+=" ,gyvena Vilniuje";
					if(ND.kalba==1)ND.atsakymas+=" ,lives in Vilnius";
					if(ND.kalba==2)ND.atsakymas+=" ,живет в Вильнюсе";
					string[] koks_rajonas = new string[] { "kokiam rajone ?", "what district ?", "в какой области ?" };
                    Console.WriteLine(koks_rajonas[ND.kalba]);
					
					if(ND.kalba==0)ND.atsakymas+=(Readline())+" rajone";
					if(ND.kalba==1)ND.atsakymas+=(Readline())+" district";
					if(ND.kalba==2)ND.atsakymas+=(Readline())+" область";
                    if (Exit) return 0;
                }
				if(ats=="Ne")
				{
					string[] koks_miestas = new string[] { "Kokiame mieste gyveni?", "In what city do you live?", "В каком городе вы живете?" };
                    Console.WriteLine(koks_miestas[ND.kalba]);
					
					
					if(ND.kalba==0)ND.atsakymas+= " , gyvena "+(Readline())+" mieste";
					if(ND.kalba==1)ND.atsakymas+= " , live in "+(Readline())+" city";
					if(ND.kalba==2)ND.atsakymas+= " , жыбёт в городе "+(Readline());
                    if (Exit) return 0;
                }
				
			}
			
			if(ND.amzius >30&&ND.amzius<=40)
			{
				string[] turi_vaiku = new string[] { "Ar turi vaiku ? Taip/Ne", "Do you have children ? Yes/No", "У вас есть дети? Да/Нет" };
                Console.WriteLine(turi_vaiku[ND.kalba]);
				string ats = tikrinam_TN(Readline());
                if (Exit) return 0;
                while (ats=="")
				{
					Console.WriteLine(klaida_str[ND.kalba]);
					ats=tikrinam_TN(Readline());
                    if (Exit) return 0;
                }
				if(ats =="Taip")
				{
					
					string[] kiek_vaiku = new string[] { "kiek turite vaiku ?", "how much you have children?", "сколько у вас есть детeи?" };
                    Console.WriteLine(kiek_vaiku[ND.kalba]);
					
					if(ND.kalba==0) atsakymas= atsakymas+" , turi "+(Readline())+" vaikus";
					if(ND.kalba==1)ND.atsakymas+=" , have a "+(Readline())+" children";
					if(ND.kalba==2)ND.atsakymas+=" , имеет "+(Readline())+" детeи";
                    if (Exit) return 0;
                }
				if(ats=="Ne")
				{
					if(ND.kalba==0)ND.atsakymas+= " , vaiku neturi";
					if(ND.kalba==1)ND.atsakymas+= " , without children";
					if(ND.kalba==2)ND.atsakymas+= " , без детей";
					
				}
				
			}
			
			if(ND.amzius >40&&ND.amzius<=50)
			{
				string[] nuomone = new string[] { "Jusu nuomone apie tos pacios lyties santuoka teigiama ar neigiama ? ", "Your opinion about same-sex marriage, whether positive or negative?", "Ваше мнение о однополых браков, положительнoе или отрицательнoе?" };
                Console.WriteLine(nuomone[ND.kalba]);
				string ats = tikrinam_TN(Readline());
                if (Exit) return 0;
                while (ats=="")
				{
					Console.WriteLine(klaida_str[ND.kalba]);
					ats=tikrinam_TN(Readline());
                    if (Exit) return 0;
                }
				if(ats =="Taip")
				{
					if(ND.kalba==0)ND.atsakymas+=" , pritaria tos pacios lyties santuokai";
					if(ND.kalba==1)ND.atsakymas+=" ,it supports the same-sex marriage";
					if(ND.kalba==2)ND.atsakymas+=" , поддерживает однополые браки";
										
				}
				if(ats=="Ne")
				{
					string[] komentaras = new string[] { "Parasyk komentara", "Write a comment", "Написать комментарий" };
                    Console.WriteLine(komentaras[ND.kalba]);
					
					
					if(ND.kalba==0)ND.atsakymas+= " , nepritaria tos pacios lyties santuokai,komentaras: "+(Readline());
                    if (ND.kalba == 1) ND.atsakymas += " , opposes same-sex marriage, comment:" + (Readline());
					if(ND.kalba==2)ND.atsakymas+= " , выступает против однополых браков, комментарий: "+(Readline());
                    if (Exit) return 0;
                }
				
			}
			
			if(ND.amzius >50&&ND.amzius<=70)
			{
				string[] turi_vaiku = new string[] { "Ar turi vaiku ? Taip/Ne", "Do you have children ? Yes/No", "У вас есть дети? Да/Нет" };
                Console.WriteLine(turi_vaiku[ND.kalba]);
				string ats = tikrinam_TN(Readline());
                if (Exit) return 0;
                while (ats=="")
				{
					Console.WriteLine(klaida_str[ND.kalba]);
					ats=tikrinam_TN(Readline());
                    if (Exit) return 0;
                }
				if(ats =="Taip")
				{
					
					string[] kiek_dukru = new string[] { "kiek turite dukru ?", "how much you have daughters ?", "сколько у вас есть дочерeй ?" };
                    Console.WriteLine(kiek_dukru[ND.kalba]);
					string dukros = Readline();
                    if (Exit) return 0;
                    if (Convert.ToInt32(dukros)>0)
					{
					if(ND.kalba==0)ND.atsakymas+=" , turi "+dukros+" dukras";
					if(ND.kalba==1)ND.atsakymas+=" , have a "+dukros+" daughters";
					if(ND.kalba==2)ND.atsakymas+=" , имеет "+dukros+" дочерeй";
					}
					string[] kiek_sunu = new string[] { "kiek turite sunu ?", "how many you have sons ?", "сколько у вас есть сыновей ?" };
                    Console.WriteLine(kiek_sunu[ND.kalba]);
					string sunus = Readline();
                    if (Exit) return 0;
                    if (Convert.ToInt32(sunus)>0&&Convert.ToInt32(dukros)==0)
					{
					if(ND.kalba==0)ND.atsakymas+=" , turi "+sunus+" sunus";
					if(ND.kalba==1)ND.atsakymas+=" , have a "+sunus+" sons";
					if(ND.kalba==2)ND.atsakymas+=" , имеет "+sunus+" сыновя" ;
					}
					if(Convert.ToInt32(sunus)>0&&Convert.ToInt32(dukros)>0)
					{
					if(ND.kalba==0)ND.atsakymas+=" , ir turi "+sunus+" sunus";
					if(ND.kalba==1)ND.atsakymas+=" , and have a "+sunus+" sons";
					if(ND.kalba==2)ND.atsakymas+=" , и имеет "+sunus+" сыновя" ;
					}
					string[] kiek_anuku = new string[] { "kiek turi anuku(berniuku) ?", "how many you have grandsons ?", "сколько имееш внуков ?" };
                    Console.WriteLine(kiek_anuku[ND.kalba]);
					string anukai = Readline();
                    if (Exit) return 0;
                    if (Convert.ToInt32(anukai)>0)
					{
					if(ND.kalba==0)ND.atsakymas+=" , turi "+anukai+" anukus";
					if(ND.kalba==1)ND.atsakymas+=" , have a "+anukai+" grandsons";
					if(ND.kalba==2)ND.atsakymas+=" , имеет "+anukai+" внуков" ;
					}
					string[] kiek_anukiu = new string[] { "kiek turi anukiu ?", "how many you have granddoughters ?", "сколько имееш внучек ?" };
                    Console.WriteLine(kiek_anukiu[ND.kalba]);
					string anukes = Readline();
                    if (Exit) return 0;
                    if (Convert.ToInt32(anukes)>0&&Convert.ToInt32(anukai)>0)
					{
					if(ND.kalba==0)ND.atsakymas+=" , ir turi "+anukes+" anukes";
					if(ND.kalba==1)ND.atsakymas+=" , and have a "+anukes+" granddoughters";
					if(ND.kalba==2)ND.atsakymas+=" , и имеет "+anukes+" внучек" ;
					}
					if(Convert.ToInt32(anukes)>0&&Convert.ToInt32(anukai)==0)
					{
					if(ND.kalba==0)ND.atsakymas+=" , turi "+anukes+" anukes";
					if(ND.kalba==1)ND.atsakymas+=" , have a "+anukes+" granddoughters";
					if(ND.kalba==2)ND.atsakymas+=" , имеет "+anukes+" внучек" ;
					}
				}
				if(ats=="Ne")
				{
					if(ND.kalba==0)ND.atsakymas+= " , vaiku neturi";
					if(ND.kalba==1)ND.atsakymas+= " , without children";
					if(ND.kalba==2)ND.atsakymas+= " , без детей";
					
				}
				
			}
			
			if(ND.amzius >70 || ND.amzius<1)
			{
				string[] sport = new string[] { "Ar vis tar sportuoji ? Taip/Ne", "Do you still training in the TAR ?Yes/No", "Вы до сих пор занимаетесь спортом ? Да/Нет" };
                Console.WriteLine(sport[ND.kalba]);
				string ats = tikrinam_TN(Readline());
                if (Exit) return 0;
                while (ats=="")
				{
					Console.WriteLine(klaida_str[ND.kalba]);
					ats=tikrinam_TN(Readline());
                    if (Exit) return 0;
                }
				if(ats =="Taip")
				{
					string[] kiek_kartu = new string[] { "Kiek kartu per savaite ?", "How many times a week?", "Cколько раз в неделю?" };
                    Console.WriteLine(kiek_kartu[ND.kalba]);
					
					if(ND.kalba==0)ND.atsakymas+=" , Vis dar sportuoja "+(Readline())+"kartus per savaite";
					if(ND.kalba==1)ND.atsakymas+=" , Still sports"+(Readline())+"times a week";
					if(ND.kalba==2)ND.atsakymas+=" , занимается спортом "+(Readline())+"раз в неделю";
                    if (Exit) return 0;
                }
              
                if (ats=="Ne")
				{
					if(ND.kalba==0)ND.atsakymas+= " , nebesportuoja ";
					if(ND.kalba==1)ND.atsakymas+= " , not involved in sports";
					if(ND.kalba==2)ND.atsakymas+= " , не занимающихся спортом ";
				}
             
			}
            return 1;	
		}
    }
}
